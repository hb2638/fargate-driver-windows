function StoreAWSTemporarySecurityCredentials {

  # Skip AWS credentials processing if their relative URI is not present.
  if ($null -eq $env:AWS_CONTAINER_CREDENTIALS_RELATIVE_URI)
  {
      return
  }

  # Create a folder to store AWS settings if it does not exist.
  $USER_AWS_SETTINGS_FOLDER = "$env:USERPROFILE\.aws"
  New-Item -Path $USER_AWS_SETTINGS_FOLDER -ItemType "directory" -Force

  # Query the unique security credentials generated for the task.
  # https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task-iam-roles.html
  $AWS_CREDENTIALS = curl.exe -L 169.254.170.2$env:AWS_CONTAINER_CREDENTIALS_RELATIVE_URI | ConvertFrom-Json -AsHashtable

  # Create a file to store the temporary credentials on behalf of the user.
  $USER_AWS_CREDENTIALS_FILE="$USER_AWS_SETTINGS_FOLDER/credentials"
  New-Item $USER_AWS_CREDENTIALS_FILE -ItemType "file" -Force

  # Set the temporary credentials to the default AWS profile.
  #
  # S3 note: if you want to sign your requests using temporary security
  # credentials, the corresponding security token must be included.
  # https://docs.aws.amazon.com/AmazonS3/latest/dev/RESTAuthentication.html#UsingTemporarySecurityCredentials
  Set-Content -Path $USER_AWS_CREDENTIALS_FILE -Encoding utf8 -NoNewline -Value @"
[default]
aws_access_key_id=$($AWS_CREDENTIALS.AccessKeyId)
aws_secret_access_key=$($AWS_CREDENTIALS.SecretAccessKey)
aws_session_token=$($AWS_CREDENTIALS.Token)
region=$($env:AWS_REGION)
"@
}

function SetUpSSH {

  # Block the container to start without an SSH public key.
  if ($null -eq $env:SSH_PUBLIC_KEY)
  {
      echo 'Need your SSH public key as the SSH_PUBLIC_KEY environment variable.'
      exit 1
  }

  #Make powershell the default shell - https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_server_configuration
  New-ItemProperty -Path "HKLM:\SOFTWARE\OpenSSH" -Name DefaultShell -Value "$env:ProgramFiles\PowerShell\7\pwsh.exe" -PropertyType String -Force

  # Copy contents from the `SSH_PUBLIC_KEY` environment variable
  # to the `${USER_SSH_KEYS_FOLDER}/authorized_keys` file.
  New-Item -Path "$env:USERPROFILE\.ssh" -ItemType "directory" -Force
  Set-Content -Path "$env:USERPROFILE\.ssh\authorized_keys" -Encoding utf8 -NoNewline -Value $env:SSH_PUBLIC_KEY
  & $env:ProgramFiles\OpenSSH\FixHostFilePermissions.ps1 -Confirm:$false

  # Clear the `SSH_PUBLIC_KEY` environment variable.
  Remove-Item -Path Env:\SSH_PUBLIC_KEY

  # HACK: sshd fails when running under this account... Get the following error
  # debug1: get_user_token - unable to generate user token for root as i am not running as system
  # We will run it under LOCALSYSTEM by using the sshd service
  echo 'Starting SSH server.'
  Start-Service -Name sshd
  echo 'Started SSH server.'
  (Get-Service -Name sshd).WaitForStatus("Stopped")
  echo 'Exiting.'
}

function AddDnsSuffix {
    # FIX: EC2 hostnames may not resolve correctly when behind a private domain
    Set-DnsClientGlobalSetting -SuffixSearchList @("testadatum.com", "test2adatum.net")
    Set-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\" -Name Domain -Value "testadatum.com"
    Set-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\" -Name "NV Domain" -Value "testadatum.com"
}


#AddDnsSuffix
StoreAWSTemporarySecurityCredentials
SetUpSSH
