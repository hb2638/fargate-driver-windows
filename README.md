# Windows based image for usage with AWS Fargate Custom Executor

A Docker image ready to run the simplest jobs with the [AWS Fargate Custom Executor
driver](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/fargate). Doesn't provide
any custom tooling nor language support.

But it will be more than enough to handle `echo "Hello world"` job properly!
